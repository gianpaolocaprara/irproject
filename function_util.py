from __future__ import division
import math
import getting_resource as gr
from mpl_toolkits.basemap import Basemap
from sklearn import metrics
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['yelp_dataset_sample']
business_coll = db['business']


# calcolo mutual information
def mutual_info(topic, category, size_list):
    lst_topic = [0] * size_list
    lst_corpora = [0] * size_list
    # relativo ai business del topic
    business = gr.get_business_topic(topic)
    # probabilita di una categoria di stare nel topic
    for element in business:
        find_com = {"business_id": element, "categories": {"$in": [category]}}
        cursor = business_coll.find(find_com, no_cursor_timeout=True)
        if cursor.count() > 0:
            i = gr.get_position_business(element)
            lst_topic[i] = 1
    # relativo ai business di tutto il corpora
    find_com_business = {"categories": {"$in": [category]}}
    projection = {"business_id": 1}
    business_cursor = business_coll.find(find_com_business, projection, no_cursor_timeout=True)
    for element in business_cursor:
        business_id = element['business_id']
        i = gr.get_position_business(business_id)
        if i:
            lst_corpora[i] = 1
    score = metrics.normalized_mutual_info_score(lst_corpora, lst_topic)
    return score


# ottiene il punto centrale di una lista di coordinate
def get_central_point(coordinates_list):
    double_x = 0
    double_y = 0
    double_z = 0
    n = 0
    for coordinate in coordinates_list:
        longitude = (coordinate[0]*math.pi) /180
        latitude =  (coordinate[1]*math.pi) / 180
        # Convert lat/lon (must be in radians) to Cartesian coordinates for each location.
        double_x += math.cos(latitude) * math.cos(longitude)
        double_y += math.cos(latitude) * math.sin(longitude)
        double_z += math.sin(latitude)
        n += 1

    avg_x = double_x/n
    avg_y = double_y/n
    avg_z = double_z/n

    # Convert average x, y, z coordinate to latitude and longitude.
    lon = math.atan2(avg_y, avg_x)
    central_square_root = math.sqrt((avg_x * avg_x) + (avg_y * avg_y))
    lat = math.atan2(avg_z, central_square_root)

    return (lat * 180) / math.pi, (lon * 180) / math.pi


#crea la mappa dove inserire i topic
def create_basemap():
    m = Basemap(projection='merc',llcrnrlat=20,urcrnrlat=60,\
            llcrnrlon=-130,urcrnrlon=-60,lat_ts=20,resolution='c')
    m.shadedrelief()
    m.drawcoastlines()
    m.drawstates()
    m.drawcountries()
    return m