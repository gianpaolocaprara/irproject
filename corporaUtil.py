from __future__ import division
import logging
import os
import operator
import getting_resource as gr
import function_util as fu
import matplotlib.pyplot as plt
from NewCorpus import NewCorpus
from gensim import corpora
from pymongo import MongoClient, ASCENDING
from geopy.distance import vincenty
from collections import Counter

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
client = MongoClient('localhost', 27017)
db = client['yelp_dataset_sample']
business_coll = db['business']
inverted_user_distribution_coll = db['invertedUserDistribution']
review_diveded_by_id_business_coll = db['reviewDivededByIDBusiness']
position_business_in_corpora_coll = db['positionBusinessInCorpora']
topics_business_distribution_coll = db['topicsBusinessDistribution']
topics_categories_distribution_coll = db['topicsCategoriesDistribution']


def create_obj(corpus_to_save, dict_to_save):
    print "Creazione degli oggetti necessari."

    # funzioni per la creazione del dizionario utenti (a ogni utente sara associato un ID univoco)
    print "Creazione dizionario utenti."
    create_dictionary('users', 'user_id', dict_to_save)
    print "Creazione dizionario utenti completato."

    print "Creazione del corpora."

    # importo la class MyCorpus per poter creare il corpus
    i = 0
    corpus_user = NewCorpus()
    for line in corpus_user:
        print str(i) + " : " + str(line)
        i += 1
    print "Creazione corpora completata."
    print "Salvataggio corpora."
    if not os.path.exists('corpus/'):
        os.makedirs('corpus/')
    corpora.MmCorpus.serialize(corpus_to_save, corpus_user)
    print "Salvataggio completato."
    print "Creazione collezione posizioni locali in corso."
    create_position_business_in_corpora()
    print "Creazione collezione completata."
    print "Creazione completata."
    return


def load(dictionary_to_load, corpus_to_load):
    print "Caricamento oggetti."
    print "Caricamento dizionario."
    dictionary = corpora.Dictionary.load(dictionary_to_load)
    print "Caricamento corpora."
    training_corpus = corpora.MmCorpus(corpus_to_load)
    print "Caricamento oggetti completato."
    return dictionary, training_corpus


def create_dictionary(coll, coll_id, file_name):
    new_list = []
    user_coll = db[coll]
    pipeline = [{
        "$project": {"_id": 0, coll_id: 1}
    }]
    rows_user = user_coll.aggregate(pipeline)
    for result in rows_user:
        new_list.append([result.values()[0]])
    rows_user.close()
    dictionary = corpora.Dictionary(new_list)
    if not os.path.exists('dict/'):
        os.makedirs('dict/')
    dictionary.save(file_name)
    return


def create_inverted_users_distribution(topic_users):
    inverted_user_distribution_coll.remove({})
    print "Creazione collezione distribuzione inversa degli utenti in corso."
    inverted_user_distribution = {}
    for element in topic_users:
        for item in element[1]:
            try:
                inverted_user_distribution[item[0]].append({'topic': element[0], 'distribution': item[1]})
            except KeyError:
                inverted_user_distribution[item[0]] = [{'topic': element[0], 'distribution': item[1]}]
    for k, v in inverted_user_distribution.items():
        json_to_insert = {'user': k, 'topics_distribution': v}
        inverted_user_distribution_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    inverted_user_distribution_coll.create_index([("user", ASCENDING)])
    inverted_user_distribution_coll.create_index([("topics_distribution", ASCENDING)])
    inverted_user_distribution_coll.create_index([("topics_distribution.distribution", ASCENDING)])
    return


def create_position_business_in_corpora():
    position_business_in_corpora_coll.remove({})
    pipeline = [
            {"$project": {"_id": 1}},
            {"$sort": {"_id": 1}}
        ]
    cursor_results = review_diveded_by_id_business_coll.aggregate(pipeline)
    # inizializzazione dizionario e contatore posizione
    position_business_dictionary = {}
    position = 0
    for element in cursor_results:
        position_business_dictionary[element.values()[0]] = position
        position += 1
    cursor_results.close()
    for k, v in position_business_dictionary.items():
        json_to_insert = {'business_id': k, 'positionInCorpora': v}
        position_business_in_corpora_coll.insert_one(json_to_insert).inserted_id
    return


def create_topic_business_distribution(corpus):
    topics_business_distribution_coll.remove({})
    print "Creazione collezione delle distribuzioni dei locali, divisi per topic."
    topic_business_distribution = {}
    i = 0
    for element in corpus:
        print "Collezione distributioni locali, divisi per topic; Iteration: " + str(i)
        # recupero l'ID ristorante associato all'indice
        business_id = gr.get_id_business(i)
        # incremento il contatore per il prossimo ID
        i += 1
        # scorro la lista delle distribuzioni dei topic del business e aggiungo ogni topic al dizionario
        for tuples in element:
            try:
                topic_business_distribution[tuples[0]].append({'business_id': business_id, 'distribution:': tuples[1]})
            except KeyError:
                topic_business_distribution[tuples[0]] = [{'business_id': business_id, 'distribution:': tuples[1]}]
    for k, v in topic_business_distribution.items():
        json_to_insert = {'topic': k, 'business_distribution': v}
        topics_business_distribution_coll.insert_one(json_to_insert).inserted_id
    print "Creazione collezione completata."
    print "Creazione indici."
    topics_business_distribution_coll.create_index([("business_distribution.business_id", ASCENDING)])
    print "Creazione indici completata."
    return


def create_categories_distribution(fcat, lcat):
    topics_categories_distribution_coll.remove({})
    print "Creo la distribuzione delle categorie del topic: "
    for id_topic in range(fcat,lcat):
        print "Recupero informazioni relative alle categorie dei locali del topic #" + str(id_topic)
        pipeline = [
                {"$match": {"topic": id_topic}},
                {"$unwind": "$business_distribution"},
                {"$lookup": {"from": "business", "localField": "business_distribution.business_id", "foreignField":
                        "business_id", "as": "embeddedData"}},
                {"$project": {"_id": 0, "embeddedData.categories": 1}}
            ]
        cursor = topics_business_distribution_coll.aggregate(pipeline)
        print "Informazioni recuperate. Inserimento categorie in lista."
        list_of_categories = []
        number_of_business_in_topic = 0
        for element in cursor:
            if element['embeddedData']:
                number_of_business_in_topic += 1
                categories = element['embeddedData'][0]['categories']
                for category in categories:
                    list_of_categories.append(category)
        print "Inserimento categorie completato. Calcolo distribuzione topic #" + str(id_topic)

        counter_list = Counter(list_of_categories).most_common(10)
        number_of_business = business_coll.count()
        category_filtered = []
        for item in counter_list:
            mi = fu.mutual_info(id_topic, item[0], number_of_business)
            if mi > 0.08:
                category_filtered.append((item[0], mi))


        print "Inserimento nel DB delle categorie relative al topic #" + str(id_topic)
        json_to_insert = {'_id': id_topic, 'categoriesDistribution': sorted(category_filtered,key=operator.itemgetter(1),reverse=True)}
        topics_categories_distribution_coll.insert_one(json_to_insert)

    return


# ottiene la mappa dei topic data una lista di topic
def get_map(topic_list):
    base_map = fu.create_basemap()
    radius_earth = 6371
    for id_topic in topic_list:
        print "Recupero informazioni relative alle coordinate dei locali del topic #" + str(id_topic)
        pipeline = [
                {"$match": {"topic": int(id_topic)}},
                {"$unwind": "$business_distribution"},
                {"$lookup": {"from": "business", "localField": "business_distribution.business_id", "foreignField":
                    "business_id", "as": "embeddedData"}},
                {"$project": {"_id": 0, "embeddedData.longitude": 1, "embeddedData.latitude": 1}}
            ]
        coordinates_list = []
        cursor = topics_business_distribution_coll.aggregate(pipeline)
        for element in cursor:
            if element['embeddedData']:
                coordinates_list.append((element['embeddedData'][0]['longitude'], element['embeddedData'][0]['latitude']))
        cursor.close()

        # ottiene il punto centrale e lo mostra sulla mappa
        avg_lat, avg_lon = fu.get_central_point(coordinates_list)
        xpt,ypt = base_map(avg_lon,avg_lat)
        base_map.plot(xpt,ypt,'ro')
        plt.text(xpt,ypt,'Topic #' + str(id_topic),color='black')
        average_coordinate = (avg_lon, avg_lat)

        sum_coordinates = 0
        for coordinate in coordinates_list:
            sum_coordinates += vincenty(average_coordinate, coordinate).kilometers

        # calcolo distanza media e il degree radius dato dalla distanza media diviso il raggio terrestre
        avg_distance = float(sum_coordinates)/float(len(coordinates_list))
        radius = float(avg_distance)/float(radius_earth)
        base_map.tissot(avg_lon,avg_lat,radius,50)
        categories = gr.get_categories(id_topic)
        print "Categorie Topic #" + str(id_topic) + ": " + str(categories)

    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed')
    plt.title("Topics Position")
    plt.show()
    return
