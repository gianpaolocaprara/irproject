from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['yelp_dataset_sample']
review_coll = db['review']
position_business_in_corpora_coll = db['positionBusinessInCorpora']
topics_business_distribution_coll = db['topicsBusinessDistribution']
topics_categories_distribution_coll = db['topicsCategoriesDistribution']


# ottiene ID del Business data la posizione
def get_id_business(position):
    business_id = ""
    cursor = position_business_in_corpora_coll.find({"positionInCorpora": position})
    for elem in cursor:
        business_id = elem['business_id']
    return business_id


def get_position_business(id_b):
    position_id = 0
    cursor = position_business_in_corpora_coll.find({"business_id": id_b})
    for elem in cursor:
            if elem['positionInCorpora']:
                position_id = elem['positionInCorpora']
    cursor.close()
    return position_id


def get_business_topic(topic):
    cursor = topics_business_distribution_coll.find({"topic": topic})
    business_result = []
    for element in cursor:
        business_result = [x['business_id'] for x in element['business_distribution']]
    cursor.close()
    return business_result


def get_user_business(user_id):
    user_business = []
    cursor = review_coll.find({"user_id": user_id}, {"_id": 0, "business_id": 1, "stars": 1})
    for element in cursor:
        user_business.append(element)
    return user_business


def get_categories(id_topic):
    cursor = topics_categories_distribution_coll.find({'_id': int(id_topic)}, {'_id': 0})
    categories = []
    for element in cursor:
        for category in element['categoriesDistribution']:
            categories.append("Category: " + str(category[0] + " MI: " + str(category[1])))
    return categories
