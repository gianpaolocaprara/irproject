import logging
import corporaUtil as corpMain
import sys
import os
from gensim import corpora, models

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',level=logging.INFO)

# sintassi comando:
# python main.py load n_topics n_words --> carica tutto il necessario per caricare gli oggetti in memoria
# (impostare anche il # di topic da utilizzare e il numero di parole (utenti) da prendere in considerazione durante LDA)
# python main.py getmap --topics topic1 topic2 ... topicN --> carica la mappa con i topic passati in input al sistema
if len(sys.argv) < 2:
    print "Bad command. Usage:"
    print "For load: python main.py load n_topics n_words\nFor get map: python main.py getmap --topics topic1 topic2 " \
          "... topicN"
    sys.exit(1)

arguments = sys.argv


def load_object(n_topics, n_words):
    # crea gli oggetti che serviranno per l'analisi
    corpMain.create_obj('corpus/corpusBusiness.mm', 'dict/users_dictionary.dict')

    # carica gli oggetti
    dictionary, training_corpus = corpMain.load('dict/users_dictionary.dict','corpus/corpusBusiness.mm')

    # calcolo LDA
    print "Calcolo LDA in corso."
    corpus_lda_model = models.ldamodel.LdaModel(training_corpus, id2word=dictionary, num_topics=n_topics)
    corpus_lda = corpus_lda_model[training_corpus]
    print "Calcolo LDA completato."

    print "Calcolo distribuzione topic sugli utenti."
    topic_users = corpus_lda_model.show_topics(num_topics=n_topics, num_words=n_words, formatted=False)
    print "Calcolo completato. Creazione indice inverso in corso."
    # creazione dell'indice inverso per conoscere la distribuzione di ogni singolo utente sui vari topic
    corpMain.create_inverted_users_distribution(topic_users)
    # creo la collezione in cui ad ogni topic ho i vari locali associati e li salvo nel DB
    corpMain.create_topic_business_distribution(corpus_lda)
    print "Salvataggio LDA."
    if not os.path.exists('lda/'):
        os.makedirs('lda/')
    corpora.BleiCorpus.serialize('lda/corpusBusiness.lda-c', corpus_lda)
    print "Salvataggio completato."
    corpMain.create_categories_distribution(0,n_topics)
    print "Finish."
    return

if arguments[1] == "load":
    print "LOAD."
    num_topics = arguments[2]
    num_words = arguments[3]
    load_object(int(num_topics), int(num_words))
elif arguments[1] == "getmap":
    print "GetMap."
    topic_list = sys.argv[sys.argv.index('--topics')+1:] # everything passed after --args
    corpMain.get_map(topic_list)
else:
    print "Bad command."
    sys.exit(1)




