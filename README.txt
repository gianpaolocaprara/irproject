Dipendenze utilizzate:

1) Libreria Gensim (versione 0.12.4; richiede libreria SciPy 0.18.0, libreria NumPy 1.10.4, libreria six 1.10.0, libreria smart-open 1.3.2) per operazioni riguardanti LDA;
2) Libreria PyMongo (versione 3.2.2) per operazioni riguardanti MongoDB;
3) Liberia Sklearn (richiede libreria scikit-learn, versione utilizzata: 0.17.1) per calcolare la mutual information delle categorie dei locali;
4) Libreria Basemap (versione 1.0.7) per poter visualizzare a video su mappa le informazioni relative ai topic;
5) Libreria Matplotlibt (versione 1.5.1) che viene utilizzata dalla libreria Basemap;
6) Libreria Geopy (versione 1.11.0) per operazioni relative alla distanza tra i locali presenti in un topic.

Istruzioni per far partire lo script:

1) Importare il dataset "yelp_dataset_sample" in MongoDB utilizzando mongorestore (utilizzare come nome del dataset il nome 'yelp_dataset_sample')
2) Far partire lo script:
	a) Caricare gli oggetti:
		- python main.py load n_topics n_words --> (n_topics = # di topic da usare in LDA; n_words = # di parole (utenti) da considerare durante l'uso di LDA)
	b) Usare la funzione di mappa per la valutazione:
		- python main.py getmap --topics topic1 topic2 ... topicN (topic1 topic2 ... topicN sono il numero dei topic di cui si vuole vedere la visualizzazione a video)
