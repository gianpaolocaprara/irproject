from gensim import corpora
from pymongo import MongoClient
from pymongo import ASCENDING

# classe che crea un nuovo corpus (dato il dataset yelp_dataset_sample presente in MongoDB)

class NewCorpus(object):
    def __init__(self):
        self.file_name = 'dict/users_dictionary.dict'
        self.dict = corpora.Dictionary.load(self.file_name)
        self.client = MongoClient('localhost', 27017)
        self.db = self.client['yelp_dataset_sample']

    def __iter__(self):
        self.createReviewDivededByID()
        reviewDivededByIDReview_coll = self.db['reviewDivededByIDBusiness']
        pipeline = [
            {"$sort" : {"_id" : 1}}
        ]
        cursor_results = reviewDivededByIDReview_coll.aggregate(pipeline)
        for results in cursor_results:
            string_resturant = "".join([(str(element['user'] + " ") * element['stars']) for element in results.values()[1]])
            yield self.dict.doc2bow(string_resturant.split())

# crea una collections in cui sono contenuti, per ogni locale, la lista delle persone che hanno commentato con il
# relativo voto (su una scala da 1 a 5)
# allow Disk Use = utilizza il disco per memorizzare i risultati
    def createReviewDivededByID(self):
        self.db.command(
            {"aggregate" :
                 "review",
             "pipeline" :
                 [{"$group" :
                       {"_id" :
                            "$business_id" , "users_votes" :
                           {"$push" :
                                {"stars" : "$stars" , "user" : "$user_id"}}}},
                  {"$out" : "reviewDivededByIDBusiness"}] , "allowDiskUse": True})
        database = self.db["reviewDivededByIDBusiness"]
        database.create_index([("business_id", ASCENDING)])
        return